# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: EUPL-1.2

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------

project = "cat4kit-documentation"
copyright = "2023 Karlsruher Institut für Technologie"
author = "Mostafa Hadizadeh"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "autodoc_traits",
    "sphinx_copybutton",
    "sphinx.ext.autodoc",
    "sphinxext.opengraph",
    "sphinxext.rediraffe",
    "sphinx.ext.intersphinx",
    "sphinx_design",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "myst_parser",
]


root_doc = "index"
source_suffix = [".md", ".rst"]
default_role = "literal"


myst_enable_extensions = ["fieldlist"]


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

show_authors = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

# html_logo = "_static/cat4kit_logo.png"
html_favicon = "_static/favicon.ico"
html_static_path = ["_static"]
html_css_files = ["custom.css"]

# pydata_sphinx_theme reference:
# https://pydata-sphinx-theme.readthedocs.io/en/latest/
html_theme = "pydata_sphinx_theme"
html_theme_options = {
    "logo": {
        "image_light": "_static/cat4kit-logo-light.png",
        "image_dark": "https://codebase.helmholtz.cloud/cat4kit/cat4kit-documentation/-/raw/main/source/_static/cat4kit-logo-dark.svg",  # noqa
    },
    "use_edit_page_button": True,
    "footer_start": ["footer"],
    "footer_end": [
        "sphinx-version",
        "theme-version",
        "last-updated",
    ],
    "gitlab_url": "https://codebase.helmholtz.cloud/cat4kit",
    # "external_links": [
    #     {"name": "TDS2STAC", "url": "https://tds2stac.readthedocs.io/"},
    #     {"name": "INTAKE2STAC", "url": "https://intake2stac.readthedocs.io/"}, # noqa
    #     {"name": "STA2STAC", "url": "https://sta2stac.readthedocs.io/"},
    #     {
    #         "name": "INSUPDEL4STAC",
    #         "url": "https://insupdel4stac.readthedocs.io/",
    #     },
    # ],
    "icon_links": [
        {
            "name": "DS2STAC",
            "url": "https://ds2stac.readthedocs.io",
            "icon": "_static/ds2stac-logo-light.png",
            "type": "local",
            # Add additional attributes to the href link.
            "attributes": {
                "target": "_blank",
                "rel": "noopener me",
                "class": "nav-link custom-fancy-css",
            },
        },
        {
            "name": "Cat4KIT-UMI",
            "url": "https://cat4kit-umi.readthedocs.io",
            "icon": "_static/cat4kit-umi-logo-light.png",
            "type": "local",
            # Add additional attributes to the href link.
            "attributes": {
                "target": "_blank",
                "rel": "noopener me",
                "class": "nav-link custom-fancy-css",
            },
        },
    ],
}

html_context = {
    "edit_page_url_template": "https://{{ codebase_url }}/\
{{ codebase_user }}/{{ codebase_repo }}/blob/\
{{ codebase_version }}/{{ doc_path }}{{ file_name }}",
    "codebase_url": "codebase.helmholtz.cloud",
    "codebase_user": "cat4kit",
    "codebase_repo": "cat4kit-documentation",
    "codebase_version": "main",
    "doc_path": "source",
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".


intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
}
