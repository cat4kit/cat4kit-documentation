..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _how_to:

===========
How to use
===========

This section will elucidate the utilization of the Cat4KIT framework for effectively managing environmental research data in your publications.

This section is divided into the following subsections:

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: How to use

   Data_portal
   api_service
